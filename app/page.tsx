import ClientOnly from "./(auth)/_components/ClientOnly";
import Container from "./(auth)/_components/Container";
import EmptyState from "./(auth)/_components/EmptyState";
import ListingCard from "./(auth)/_components/listings/ListingCard";

import getCurrentUser from "./actions/getCurrentUser";
import getListings, { IListingsParams } from "./actions/getListing";

interface HomeProps {
  searchParams: IListingsParams;
}

const Home = async ({ searchParams }: HomeProps) => {
  const listings = await getListings(searchParams);
  const currentUser = await getCurrentUser();

  if (listings.length === 0) {
    return (
      <ClientOnly>
        <EmptyState showReset />
      </ClientOnly>
    );
  }

  return (
    <ClientOnly>
      <Container>
        <div className="pt-24 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6 gap-8">
          {listings.map((listing) => {
            return (
              <div>
                <ListingCard
                  key={listing.id}
                  data={listing}
                  currentUser={currentUser}
                />
              </div>
            );
          })}
        </div>
      </Container>
    </ClientOnly>
  );
};

export default Home;
