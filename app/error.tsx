"use client";

import { useEffect } from "react";
import EmptyState from "./(auth)/_components/EmptyState";

interface ErrorStateProps {
  error: Error;
}

const ErrorState: React.FC<ErrorStateProps> = ({ error }) => {
  useEffect(() => {
    console.error(error);
  }, [error]);

  return <EmptyState title="404 | Not found" subtitle="Something Went Wrong" />;
};

export default ErrorState;
