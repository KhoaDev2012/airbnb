import Loader from "./(auth)/_components/Loader";

const Loading = () => {
  return (
    <div>
      <Loader />
    </div>
  );
};

export default Loading;
