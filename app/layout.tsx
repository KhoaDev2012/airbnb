import type { Metadata } from "next";
import { Inter } from "next/font/google";

import Navbar from "./(auth)/_components/Navbar/Navbar";
import ClientOnly from "./(auth)/_components/ClientOnly";
import LoginModal from "./(auth)/login/page";
import RegisterModal from "./(auth)/register/page";
import RentModal from "./(auth)/rent/page";
import SearchModal from "./(auth)/search/page";

import getCurrentUser from "./actions/getCurrentUser";

import ToasterProvider from "./providers/ToasterProvider";
import { ThemeProvider } from "./providers/ThemeProvider";

import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "My Airbnb App",
  description: "Airbnb Clone",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const currentUser = await getCurrentUser();

  return (
    <html lang="vn">
      <body className={inter.className}>
        <ThemeProvider
          attribute="class"
          defaultTheme="dark"
          enableSystem
          disableTransitionOnChange
        >
          <ClientOnly>
            <ToasterProvider />
            <LoginModal />
            <RegisterModal />
            <RentModal />
            <SearchModal />
            <Navbar currentUser={currentUser} />
          </ClientOnly>
          <div className="pb-20 pt-28">{children}</div>
        </ThemeProvider>
      </body>
    </html>
  );
}
