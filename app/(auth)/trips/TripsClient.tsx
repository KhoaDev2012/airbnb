"use client";

import axios from "axios";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { useCallback, useState } from "react";

import { SafeReservation, SafeUser } from "@/app/types";

import Heading from "../_components/Heading";
import Container from "../_components/Container";
import ListingCard from "../_components/listings/ListingCard";

interface TripsCLientProps {
  reservations: SafeReservation[];
  currentUser?: SafeUser | null;
}

const TripsCLient: React.FC<TripsCLientProps> = ({
  reservations,
  currentUser,
}) => {
  const router = useRouter();
  const [deletingId, setDeletingId] = useState("");

  const onCancel = useCallback(
    (id: string) => {
      setDeletingId(id);

      axios
        .delete(`/api/reservations/${id}`)
        .then(() => {
          toast.success("Reservation Canceled");
          router.refresh();
        })
        .catch((error) => {
          toast.error(error?.response?.data?.error);
        })
        .finally(() => {
          setDeletingId("");
        });
    },
    [router]
  );

  return (
    <div>
      <Container>
        <Heading title="Trips" subtitle="Where you want to go" />
        <div className="mt-10 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6 gap-8">
          {reservations.map((reservation) => (
            <ListingCard
              key={reservation.id}
              data={reservation.listing}
              actionId={reservation.id}
              onAction={onCancel}
              reservation={reservation}
              disabled={deletingId === reservation.id}
              currentUser={currentUser}
              actionLabel="Cancel reservation"
            />
          ))}
        </div>
      </Container>
    </div>
  );
};

export default TripsCLient;
