"use client";

import React from "react";

import { SafeUser } from "@/app/types";

import Container from "../Container";
import Logo from "./Logo";
import Search from "./Search";
import UserMenu from "./UserMenu";
import Categories from "./Categories";
import { ToggleTheme } from "../ToggleTheme";

interface NavbarProps {
  currentUser?: SafeUser | null;
}

const Navbar: React.FC<NavbarProps> = ({ currentUser }) => {
  return (
    <div className="fixed w-full z-10 shadow-sm backdrop-blur-lg">
      <div className="py-4 border-b-[1px]">
        <Container>
          <div className="flex flex-row items-center justify-between gap-3">
            <Logo />
            <Search />
            <div className="flex items-center justify-center gap-3">
              <ToggleTheme />
              <UserMenu currentUser={currentUser} />
            </div>
          </div>
        </Container>
      </div>

      {/* CATEGORY */}
      <Categories />
    </div>
  );
};

export default Navbar;
