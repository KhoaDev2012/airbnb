"use client";

import { useEffect, useState } from "react";

interface ClientOnlyProps {
  children: React.ReactNode;
}

// The Client Only used to protect component which exist a HYDRA ERROR in NEXTJS

const ClientOnly: React.FC<ClientOnlyProps> = ({ children }) => {
  const [hasMounted, setHasMouted] = useState(false);

  useEffect(() => {
    setHasMouted(true);
  }, []);

  if (!setHasMouted) {
    return null;
  }

  return <>{children}</>;
};

export default ClientOnly;
