"use client";

import { IconType } from "react-icons";

interface ButtonProps {
  label: string;
  onCLick: (e: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
  outline?: boolean;
  small?: boolean;
  icon?: IconType;
}

const Button: React.FC<ButtonProps> = ({
  label,
  onCLick,
  disabled,
  outline,
  small,
  icon: Icon,
}) => {
  return (
    <button
      onClick={onCLick}
      disabled={disabled}
      className={`relative disabled:opacity-70 disabled:cursor-not-allowed rounded-lg hover:bg-rose-500 hover:text-white transition w-full 
        ${
          outline
            ? "bg-rose-500 border-rose-500 text-white hover:opacity-90 dark:bg-black dark:text-white dark:hover:bg-rose-500 dark:hover:border-rose-500 dark:border-black"
            : "bg-white border-rose-500 text-rose-500"
        }
        ${
          small
            ? "py-1 text-md font-medium border-[1px] dark:text-black dark:border-white dark:hover:text-white dark:hover:border-rose-500 "
            : "py-3 text-md font-semibold border-2"
        }
        
        `}
    >
      {Icon && <Icon size={24} className="absolute left-4 top-3" />}
      {label}
    </button>
  );
};

export default Button;
