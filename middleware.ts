export { default } from "next-auth/middleware";

// Định nghĩa các route cần bảo vệ
export const config = {
  matcher: ["/trips", "/favorites", "/reservations", "/properties"],
};
